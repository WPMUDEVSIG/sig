# The WPMU DEV Special Interest Group

This repo is for the SIG itself, issues, wiki and other docs, updates, info, etc.

Starting information can be found in the repo [Wiki](https://bitbucket.org/WPMUDEVSIG/sig/wiki/Home), including links to other sites to discuss SIG activity.

To participate, you absolutely must understand [How This SIG Works](https://bitbucket.org/WPMUDEVSIG/sig/wiki/How_This_SIG_Works). Please ask questions in Slack or in the Google Group if you are not sure about our workflow. 